import React from 'react';

import Construction from '../media/construction.png';
import MeasureIt from '../media/measureIt.png';

function Home() {
	return (
		<div className="Home">

			<div className="Title">
				<h1>SafetyCheckOk</h1>
			</div>

			<div className="StatusImg">
				<img src={MeasureIt} alt="Girl Measuring" />
			</div>

			<div className="Status">
				<p>Website under construction</p>
			</div>

			<div className="Construction">
				<img src={Construction} alt="Construction" />
			</div>

		</div>
	);
}

export default Home;
